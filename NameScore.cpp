#include <iostream>
#include <string>

using namespace std;

class Hero
{

public:

    void SetHero(string name, int score)
    {
        m_name = name;
        m_score = score;
    }

    string GetName() const
    {
        return m_name;
    }

    int GetScore() const
    {
        return m_score;
    }

private:

    string m_name;
    int m_score = 0;
};

int main()
{
    int countP = 0;
    string playerN;
    int playerS;

    cout << "Enter the number of players > 0: \n";
    cin >> countP;
    if (countP <= 0)
    {
        cout << "No valid number! \n";
        return 1; 
    }

    Hero* hArr = new Hero[countP];

    for (int i = 0; i < countP; ++i) 
    {
        cout << "Get player Name " << i + 1 << ": ";
        cin >> playerN;
        cout << "Get player Score " << i + 1 << ": ";
        cin >> playerS;
        hArr[i].SetHero(playerN, playerS);
    }

    for (int i = 1; i < countP; ++i) 
    {
        for (int j = countP - 1; j >= i; j--)
        { 
            if (hArr[j - 1].GetScore() < hArr[j].GetScore())
            {
                swap(hArr[j - 1], hArr[j]);
            }
        }   
    }
    cout << "\n";

    for (int i = 0; i < countP; ++i) 
    {
        cout << "Player " << i + 1 << " "
            << hArr[i].GetName() << " " << hArr[i].GetScore() << "\n";
    }

    delete[] hArr;
}
